#!/usr/bin/env bash
#
# Cria o banco de dados da aplicação no MySQL.

USERNAME=$1
PASSWORD=$2
DATABASE=$3

mysqladmin -u${USERNAME} -p${PASSWORD} create ${DATABASE};
mysql -u${USERNAME} -p${PASSWORD} --execute="CREATE TABLE emails (email VARCHAR(300) NOT NULL UNIQUE);" ${DATABASE}

# Jarbas

Sistema para cadastro de e-mails para uma lista de *newsletter*
com objetivo de mostrar ciclos de TDD.

# Usando

Você precisa ter o [Vagrant][1] e o [VirtualBox][2] instalados
em sua máquina já. Clone o respositório, navegue até ele usando
o terminal de sua preferência e execute:

    vagrant up

A aplicação web pode ser acessada através de: http://192.168.42.2.xip.io/


[1]: http://www.vagrantup.com
[2]: https://www.virtualbox.org

<?php

namespace Jarbas\Storage;

class RelationalTest extends \PHPUnit_Extensions_Database_TestCase
{
    private $connection = null;
    private static $pdo = null;

    public function getConnection()
    {
        if (null === $this->connection) {
            if (null === self::$pdo) {
                self::$pdo = new \PDO('sqlite::memory:');
                self::$pdo->exec('CREATE TABLE emails (email TEXT UNIQUE)');
            }

            $this->connection = $this->createDefaultDBConnection(self::$pdo, ':memory:');
        }

        return $this->connection;
    }

    public function getDataSet()
    {
        return $this->createXMLDataSet(__DIR__.'/emails.xml');
    }

    public function testSaveShouldReturnTrueWhenEmailIsInserted()
    {
        $relational = new Relational(self::$pdo);
        $email = 'nelson@jarbas.com';
        $this->assertTrue($relational->save($email));
    }

    public function testSaveShouldReturnFalseWhenEmailAlreadyExists()
    {
        $relational = new Relational(self::$pdo);
        $existingEmail = 'jarbas@example.org';
        $this->assertFalse($relational->save($existingEmail));
    }
}

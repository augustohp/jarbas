<?php

namespace Jarbas\Controller;

use Symfony\Component\HttpFoundation\Request;

class SubscribeTest extends \PHPUnit_Framework_TestCase
{
    private $storage = null;
    private $controller = null;

    protected function setUp()
    {
        $this->storage = $this->getMock('Jarbas\StorageInterface');
        $validator = new \Jarbas\Validator\Email;
        $this->controller = new Subscribe($this->storage, $validator);
    }

    public function testSubscribeActionShoulReturnSuccessMessageWhenEmailWasRegistered()
    {
        $email = 'jarbas@jarbas.org';

        $storage = $this->storage;
        $storage->expects($this->once())
            ->method('save')
            ->with($email)
            ->will($this->returnValue(true));

        $response = $this->makeFakeRequest($email);

        $this->assertContains('Aguarde nossas novidades!', $response->getContent());
    }

    public function testSubscribeActionShouldReturnFailureMessageWhenEmailCouldNotBeStored()
    {
        $email = 'jarbas@jarbas.org';

        $storage = $this->storage;
        $storage->expects($this->once())
            ->method('save')
            ->with($email)
            ->will($this->returnValue(false));

        $response = $this->makeFakeRequest($email);

        $this->assertContains('Não foi possível cadastrar o email', $response->getContent());
    }

    public function testSubscribeActionShouldReturn()
    {
        $email = 'jarbas';

        $storage = $this->storage;
        $storage->expects($this->never())
            ->method('save');

        $response = $this->makeFakeRequest($email);

        $this->assertContains('E-mail inválido', $response->getContent());
    }

    private function makeFakeRequest($email)
    {
        $request = new Request(array(), array('email' => $email));

        return $this->controller->post($request);
    }
}

<?php

namespace Jarbas\Storage;

class PDOTest extends \PHPUnit_Framework_TestCase
{
    public function testSave()
    {
        $this->markTestSkipped('Não façam isso em casa!');

        $pdo = $this->getMockBuilder('\PDO')->disableOriginalConstructor()->getMock();
        $pdo->expects($this->once())
            ->method('prepare')
            ->with('INSERT INTO emails (email) VALUES (?)')
            ->will($this->returnValue($pdoStatementMock));

        $relationalStorage = new PDO($pdo);

        $this->assertTrue($relationalStorage->save($data));
    }
}

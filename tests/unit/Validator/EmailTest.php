<?php

namespace Jarbas\Validator;

class EmailTest extends \PHPUnit_Framework_TestCase
{
    public function test_isvalid_returns_true_when_email_is_valid()
    {
        $validator = new Email;

        $this->assertTrue($validator->isValid('john@example.com'));
    }

    public function test_isValid_returns_false_when_email_is_invalid()
    {
        $validator = new Email;

        $this->assertFalse($validator->isValid('not-an-email'));
    }
}

<?php

class EndToEndTest extends Silex\WebTestCase
{
    public function createApplication()
    {
        $applicationConfiguration = array(
            'database.dsn' => 'sqlite::memory:',
            'database.username' => null,
            'database.password' => null
        );
        $app = \Jarbas\Application::create($applicationConfiguration);

        $app['debug'] = true;
        $app['exception_handler']->disable();

        return $app;
    }


    private function setUpDatabase(\PDO $connection)
    {
        $connection->exec('CREATE TABLE emails (email TEXT UNIQUE)');
    }

    public function testRegisterAlertsUserWhenRegistrationFails()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/');
        $this->assertTrue($client->getResponse()->isOk(), 'Home route not found');
        $this->assertCount(1, $crawler->filter('#email'), 'Email field not found');
        $this->assertCount(1, $crawler->selectButton('Cadastrar'), 'Cadastrar button not found');

        $form = $crawler->selectButton('Cadastrar')->form();
        $form['email'] = 'jarbas@jarbas.com';

        $crawler = $client->submit($form);

        $this->assertContains('Não foi possível cadastrar o email', $client->getResponse()->getContent());
    }

    public function testRegisterAlertsUserWhenEmailIsInvalid()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/');
        $this->assertTrue($client->getResponse()->isOk(), 'Home route not found');
        $this->assertCount(1, $crawler->filter('#email'), 'Email field not found');
        $this->assertCount(1, $crawler->selectButton('Cadastrar'), 'Cadastrar button not found');

        $form = $crawler->selectButton('Cadastrar')->form();
        $form['email'] = 'jarbas';

        $crawler = $client->submit($form);

        $this->assertContains('E-mail inválido', $client->getResponse()->getContent());
    }

    public function testRegisterAlertsUserWhenEmailIsRegistered()
    {
        $client = $this->createClient();
        $this->setUpDatabase($this->app['pdo']);
        $crawler = $client->request('GET', '/');
        $this->assertTrue($client->getResponse()->isOk(), 'Home route not found');
        $this->assertCount(1, $crawler->filter('#email'), 'Email field not found');
        $this->assertCount(1, $crawler->selectButton('Cadastrar'), 'Cadastrar button not found');

        $form = $crawler->selectButton('Cadastrar')->form();
        $form['email'] = 'jarbas@example.org';

        $crawler = $client->submit($form);

        $this->assertContains('Aguarde nossas novidades!', $client->getResponse()->getContent());
    }
}

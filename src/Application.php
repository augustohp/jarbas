<?php

namespace Jarbas;

use Symfony\Component\HttpFoundation;

class Application
{
    public static function create(array $values)
    {
        $application = new \Silex\Application($values);
        $application->register(
            new \Silex\Provider\TwigServiceProvider,
            array('twig.path' => __DIR__ . '/../web/templates/')
        );

        $application->register(new \Silex\Provider\ServiceControllerServiceProvider());

        $application['pdo'] = $application->share(function($application) {
            $dsn = $application['database.dsn'];
            $username = $application['database.username'];
            $password = $application['database.password'];

            $pdo = new \PDO($dsn, $username, $password);
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            return $pdo;
        });
        $application['storage'] = $application->share(function () use ($application) {
            return new Storage\Relational($application['pdo']);
        });

        $application->get('/', function() use ($application) {
            return $application['twig']->render('form.twig.html');
        });

        $application['subscribe.controller'] = $application->share(function() use ($application) {
            return new Controller\Subscribe($application['storage'], new Validator\Email);
        });

        $application->post('/subscribe', 'subscribe.controller:post');

        return $application;
    }
}

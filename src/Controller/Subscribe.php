<?php

namespace Jarbas\Controller;

use Jarbas\StorageInterface;
use Jarbas\Validator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Subscribe
{
    private $storage = null;
    private $validator = null;

    public function __construct(StorageInterface $storage, Validator\Email $validator)
    {
        $this->storage = $storage;
        $this->validator = $validator;
    }

    public function post(Request $request)
    {
        $email = $request->get('email');

        if (false === $this->validator->isValid($email)) {
            return new Response('E-mail inválido', 400);
        }

        if ($this->storage->save($email)) {
            return new Response('Aguarde nossas novidades!', 201);
        }

        return new Response('Não foi possível cadastrar o email', 500);
    }
}

<?php

namespace Jarbas;

interface StorageInterface
{
    public function save($data);
}

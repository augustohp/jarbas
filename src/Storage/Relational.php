<?php

namespace Jarbas\Storage;

use PDO;
use Jarbas\StorageInterface;

class Relational implements StorageInterface
{
    public function __construct(PDO $connection)
    {
        $this->pdo = $connection;
    }

    public function save($data)
    {
        try {
            $insertStatement = $this->pdo->prepare('INSERT INTO emails (email) VALUES (?)');
            $insertStatement->execute([$data]);

            return true;
        } catch (\PDOException $e) {
            return false;
        }
    }
}

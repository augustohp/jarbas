<?php

namespace Jarbas\Validator;

class Email
{
    public function isValid($value)
    {
        return (false !== filter_var($value, \FILTER_VALIDATE_EMAIL));
    }
}
